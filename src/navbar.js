import homeimg1 from '../src/media/Logo (1).png'


const Navbar= () => {
    return (
        <header>
            <img id='logo' src={homeimg1} alt="logo" />
            <h5>
                Pinhead<br />Analytics
            </h5>
        <nav >
          <ul className="nav_links">
            <li><a href="">Home</a></li>
            <li><a href="">Services</a></li>
            <li><a href="">Blog</a></li>
            <li><a href="">Contact Us</a></li>
          </ul>
        </nav>
        
        </header>
        
        
    );
}
export default Navbar;