
import Navbar from './navbar';
import Home from './home';
import Footer from './footer';


function App() {


  return (
    <div className="App">
      <Navbar />
      <Home />
      <Footer />
    </div>
  );
}

export default App;
