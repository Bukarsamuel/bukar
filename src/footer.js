const Footer= () => {
    return (
       <footer className="footer">
        <div className="container">
            <div className="row">
                <div className="footer-col">
                    <h4>Pinhead Analytics</h4>
                        <ul>
                            <p>Lorem ipsum dolor amet consectetur adipielit sed eiusm tempor incididunt ut labore dolore.</p>
                            <li><a href=""> </a></li>
                        </ul>
                </div>
                <div className="footer-col">
                    <h4>Important Links</h4>
                        <ul>
                            <li><a href=""> Home</a></li>
                            <li><a href=""> Services</a></li>
                            <li><a href=""> Blog</a></li>
                            <li><a href=""> Contact Us</a></li>
                        </ul>
                </div>
                <div className="footer-col">
                    <h4>Featured Services</h4>
                        <ul>
                            <li><a href=""> Tanlent Services</a></li>
                            <li><a href=""> Asia Gtm Parthnership</a></li>
                            <li><a href=""> Investment And Advisari</a></li>
                        </ul>
                </div>
                <div className="footer-col">
                    <h4>Contact Info</h4>
                        <ul>
                            <li><a href=""> +91 93056 79342</a></li>
                            <li><a href=""> info@ufinity.in</a></li>
                        </ul>
                </div>
            </div>
        </div>
       </footer>
        
    );
}
export default Footer;